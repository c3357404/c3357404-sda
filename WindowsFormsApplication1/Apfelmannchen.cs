﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Timers;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Apfelmannchen : Form
    {
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private static int j = 0;
        private Image picture;
        private Graphics g1, g2;
        private Cursor c1, c2;

        System.Windows.Forms.Timer colourTimer = new System.Windows.Forms.Timer();


        // For handling clicks, drags, etc.
        private bool hasClicked;
        private Point currentPoint = new Point();
        private Point previousPoint = new Point();

        

        public Apfelmannchen()
        {
            InitializeComponent();   
            init();
            start();

            this.DoubleBuffered = true;
            this.Cursor = Cursors.Cross;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var windowG = e.Graphics;

            windowG.DrawImage(picture, 0, 0);

        }

        private void Apfelmannchen_Load(object sender, EventArgs e)
        {
        }

        public void init()
        {
            finished = false;
            Size = new Size(640, 480);
            x1 = Size.Width;
            y1 = Size.Height;
            xy = (float)x1 / (float)y1;

            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);
            finished = true;
        }

        public void start()
        {
            action = false;
            rectangle = false;
            InitValues();
            xzoom = (yende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            CreateMenu();
            DrawApfelmannchen();
        }

        public void InitValues()
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
            {
                xstart = xende - (yende - ystart) * (double)xy;
            }
        }

        public void DrawApfelmannchen()
        {
            int x, y;
            float h, b, alt = 0.0f;
            Pen p = new Pen(Color.Black);

            action = false;

            // set the cursor somwhere on the screen here (line 184, Fractal.java)
            // show status (line 185, Fractal.java)
            // g1.DrawString("Mandelbrot-Set will be produce - Please Wait...", Font.Bold, Brush, (float)1, (float)1);

            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = PointColour(xstart + xzoom * (double)x, ystart + yzoom * (double)y, j);

                    if (h != alt)
                    {
                        b = 1.0f - h * h;

                        HSBColor color = new HSBColor(h * 255, 0.8f * 255, b * 255);
                        p = new Pen(color.Color);

                        alt = h;
                    }
                    g1.DrawLine(p, x, y, x + 1, y);
                }

            action = true;
        }

        public void Paint(Graphics g)
        {
            Update(g);
        }

        public void Update(Graphics g)
        {
            Pen p = new Pen(Color.Black, 2);
            g.DrawImage(picture, 0, 0);
            if (rectangle)
            {
                // set color to white

                if (xs < xe)
                {
                    if (ys < ye)
                    {
                        g.DrawRectangle(p, xs, ys, (xe - xs), (ye - ys));
                    }
                    else
                    {
                        g.DrawRectangle(p, xs, ye, (xe - xs), (ys - ye));
                    }
                }
                else
                {
                    if (ys < ye)
                    {
                        g.DrawRectangle(p, xe, ys, (xs - xe), (ye - ys));
                    }
                    else
                    {
                        g.DrawRectangle(p, xe, ye, (xs - xe), (ys - ye));
                    }
                }
                DrawApfelmannchen();
                Refresh();
            }
        }        

        public void destroy()
        {
            if (finished)
            {
                //removeMouseListener(this);
                //removeMouseMotionListener(this);
                picture = null;
                g1 = null;
                c1 = null;
                c2 = null;
                GC.Collect();
            }
        }

        public void Stop()
        {
        }

        private float PointColour(double xwert, double ywert, int j)
        {
            double r = 0.0, i = 0.0, m = 0.0;
            // SN: Remove to create a global variable to handle colour changing
            //int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }

            return (float)j / (float)MAX;
        }

        

        public void CycleColours(object source, EventArgs e)
        {
            if (j < 255)
            {
                j++;
                DrawApfelmannchen();
                Refresh();
            }
            else if (j == 255)
            {
                j = 0;
                DrawApfelmannchen();
                Refresh();
            }
        }

        #region MouseMovement
        private void Apfelmannchen_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;

            if (action)
            {
                xe = e.X;
                ye = e.Y;

                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }

                w = (xe - xs);
                z = (ye - ys);

                if ((w < 2) && (z < 2))
                {
                    InitValues();
                }
                else
                {
                    if (((float)w > (float)z * xy))
                    {
                        ye = (int)((float)ys + (float)w / xy);
                    }
                    else
                    {
                        xe = (int)((float)xs + (float)z * xy);
                    }
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }

                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;

                Paint(g1);
                rectangle = false;
            }
        }

        private void Apfelmannchen_MouseDown(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xs = e.X;
                ys = e.Y;
                Refresh();
            }
        }

        //public void Apfelmannchen_MouseMove(Object sender, MouseEventArgs e)
        //{
        //    Point point = new Point(e.X, e.Y);
        //    // If we "have the mouse", then we draw our lines.
        //    //if (bHaveMouse)
        //    //{
        //    // If we have drawn previously, draw again in
        //    // that spot to remove the lines.
        //    if (previousPoint.X != -1)
        //    {
        //        DrawRectangle(currentPoint, previousPoint);
        //    }
        //    // Update last point.
        //    previousPoint = point;
        //    // Draw new lines.
        //    DrawRectangle(currentPoint, point);
        //    //}
        //}

        public void Apfelmannchen_MouseMove(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;

                Refresh();
            }
        }

        private void DrawRectangle(Point pointX, Point pointY)
        {
            Rectangle rect = new Rectangle();

            pointX = PointToScreen(pointX);
            pointY = PointToScreen(pointY);

            if (pointX.X < pointY.X)
            {
                rect.X = pointX.X;
                rect.Width = pointY.X - pointX.X;
            }
            else
            {
                rect.X = pointY.X;
                rect.Width = pointX.X - pointY.X;
            }

            if (pointX.Y < pointY.Y)
            {
                rect.Y = pointX.Y;
                rect.Height = pointY.Y - pointX.Y;
            }
            else
            {
                rect.Y = pointY.Y;
                rect.Height = pointX.Y - pointY.Y;
            }

            ControlPaint.DrawReversibleFrame(rect, Color.Blue, FrameStyle.Thick);
        }
        #endregion

        #region Menu
        public void CreateMenu()
        {
            // Create a main menu object.
            MainMenu mainMenu = new MainMenu();

            // Create empty menu item objects.
            MenuItem fileMenuItem = new MenuItem();            
            MenuItem fileMenuItemSave = new MenuItem();

            MenuItem colourMenuItem = new MenuItem();
            MenuItem colourMenuItemRed = new MenuItem();
            MenuItem colourMenuItemBlue = new MenuItem();
            MenuItem colourMenuItemGreen = new MenuItem();
            MenuItem colourMenuItemCycleStart = new MenuItem();
            MenuItem colourMenuItemCycleStop = new MenuItem();

            // Set the caption of the menu items.
            fileMenuItem.Text = "&File";
            fileMenuItemSave.Text = "&Save";

            colourMenuItem.Text = "&Colours";
            colourMenuItemRed.Text = "&Original";
            colourMenuItemBlue.Text = "&Blue";
            colourMenuItemGreen.Text = "&Green";
            colourMenuItemCycleStart.Text = "&Start Cycle";
            colourMenuItemCycleStop.Text = "&Stop Cycle";

            // Add the menu items to the main menu.
            mainMenu.MenuItems.Add(fileMenuItem);
            fileMenuItem.MenuItems.Add(fileMenuItemSave);

            mainMenu.MenuItems.Add(colourMenuItem);
            colourMenuItem.MenuItems.Add(colourMenuItemRed);
            colourMenuItem.MenuItems.Add(colourMenuItemBlue);
            colourMenuItem.MenuItems.Add(colourMenuItemGreen);
            colourMenuItem.MenuItems.Add(colourMenuItemCycleStart);
            colourMenuItem.MenuItems.Add(colourMenuItemCycleStop);

            // Add functionality to the menu items using the Click event. 
            fileMenuItemSave.Click += new EventHandler(this.SaveMenuItem_Click);

            colourMenuItemRed.Click += new EventHandler(this.ColourRedMenuItem_Click);
            colourMenuItemBlue.Click += new EventHandler(this.ColourBlueMenuItem_Click);
            colourMenuItemGreen.Click += new EventHandler(this.ColourGreenMenuItem_Click);
            colourMenuItemCycleStart.Click += new EventHandler(this.ColourCycleStartMenuItem_Click);
            colourMenuItemCycleStop.Click += new EventHandler(this.ColourCycleStopMenuItem_Click);

            // Assign mainMenu1 to the form.
            this.Menu = mainMenu;
        }
         
        // SN: Click event handler for the save button in the menu
        public void SaveMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.InitialDirectory = "C:\\";
                sfd.Filter = "BMP Files|*.bmp";
                if (sfd.ShowDialog(this) == DialogResult.OK)
                {
                    picture.Save(sfd.FileName, ImageFormat.Bmp);
                }
            }
        }

        // SN: Click event handler for original Das Apfelmannchen colour
        public void ColourRedMenuItem_Click(object sender, EventArgs e)
        {
            j = 0;
            DrawApfelmannchen();
            Refresh();
        }

        // SN: Click event handler for turning Das Apfelmannchen blue
        public void ColourBlueMenuItem_Click(object sender, EventArgs e)
        {
            j = 150;
            DrawApfelmannchen();
            Refresh();
        }

        // SN: Click event handler for turning Das Apfelmannchen green
        public void ColourGreenMenuItem_Click(object sender, EventArgs e)
        {
            j = 50;
            DrawApfelmannchen();
            Refresh();
        }

        // SN: Click event handler for cycling through colours for Das Apfelmannchen
        public void ColourCycleStartMenuItem_Click(object sender, EventArgs e)
        {
            colourTimer.Tick += new EventHandler(CycleColours);
            colourTimer.Interval = 10;
            colourTimer.Start();
        }

        // SN: Click event handler for stopping the cycling of colours for Das Apfelmannchen
        public void ColourCycleStopMenuItem_Click(object sender, EventArgs e)
        {
            colourTimer.Stop();
        }
        #endregion

        protected override void OnLoad(EventArgs e)
        {
            MouseDown += new MouseEventHandler(Apfelmannchen_MouseDown);
            MouseUp += new MouseEventHandler(Apfelmannchen_MouseUp);
            MouseMove += new MouseEventHandler(Apfelmannchen_MouseMove);
        }
    }
}
